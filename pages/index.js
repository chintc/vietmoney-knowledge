import Head from 'next/head'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer'
import styles from '../styles/Home.module.css'

export default function Home() {
	return (
		<>
			<div>
				<Head>
					<meta charSet='utf-8' />
					<meta name='viewport' content='width=device-width, initial-scale=1' />
					<title>Knowledge Base HTML Template</title>
					<link rel='icon' href='/assets/img/favicon.png' type='image/x-icon' />
					<link
						href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600'
						rel='stylesheet'
					/>
					<script type='text/javascript' src='/static/jquery.js'></script>
					<script type='text/javascript' src='/static/main.js'></script>
					{/* <link rel='stylesheet' href='/static/main.css' /> */}
				</Head>

				<div
					className='section-hero uk-background-blend-color-burn uk-background-top-center uk-background-cover uk-section-large1 cta'
					style={{
						backgroundImage:
							'url(https://images.pexels.com/photos/2928796/pexels-photo-2928796.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500)',
					}}
				>
					<nav className='uk-navbar-container uk-margin uk-navbar-transparent uk-light'>
						<div className='uk-container'>
							<div uk-navbar>
								<div className='uk-navbar-left'>
									<a
										className='uk-navbar-item uk-logo uk-text-uppercase'
										href='index.html'
									>
										<span
											className='uk-margin-small-right'
											uk-icon='icon: lifesaver'
										/>{' '}
										Knowledge
									</a>
								</div>
								<div className='uk-navbar-right'>
									<ul className='uk-navbar-nav uk-text-uppercase uk-visible@m uk-margin-medium-left'>
										<li>
											<a href='index.html'>Home</a>
										</li>
										<li>
											<a href='article.html'>Article</a>
											<div className='uk-navbar-dropdown'>
												<ul className='uk-nav uk-navbar-dropdown-nav'>
													<li>
														<a href='article.html'>Scrollspy</a>
													</li>
													<li>
														<a href='article-narrow.html'>Narrow</a>
													</li>
												</ul>
											</div>
										</li>
										<li>
											<a href='faq.html'>Faq</a>
										</li>
										<li>
											<a href='contact.html'>Contact</a>
										</li>
										<li>
											<a href='components.html'>Components</a>
										</li>
									</ul>
									<a
										className='uk-navbar-toggle uk-hidden@m'
										href='#offcanvas'
										uk-navbar-toggle-icon
										uk-toggle
									/>
								</div>
							</div>
						</div>
					</nav>
					<div className='uk-container hero'>
						<h1 className='uk-heading-primary uk-text-center uk-margin-large-top uk-light'>
							Create a Knowledge Base with Ease
						</h1>
						<p className='uk-text-lead uk-text-center uk-light'>
							Lead volutpat nibh ligula gravida. Magna auctor eget venenatis
							phasellus luctus sodales pulvinar
						</p>
						<div className='uk-flex uk-flex-center uk-inliner'>
							<form className='uk-margin-medium-top uk-margin-xlarge-bottom uk-search uk-search-default'>
								<a href className='uk-search-icon-flip' uk-search-icon />
								<input
									id='autocomplete'
									className='uk-search-input uk-form-large'
									type='search'
									autoComplete='off'
									name='s'
									placeholder='Enter search term here'
								/>
							</form>
						</div>
					</div>
				</div>
				<div className='uk-section'>
					<div className='uk-container'>
						<div
							className='uk-child-width-1-3@s uk-grid-match uk-grid-medium uk-text-center'
							uk-grid
						>
							<div>
								<a href='category.html' className='box uk-border-rounded'>
									<h3>Quick Setup</h3>
								</a>
							</div>
							<div>
								<a href='category.html' className='box uk-border-rounded'>
									<h3>Administration Panel</h3>
								</a>
							</div>
							<div>
								<a href='category.html' className='box uk-border-rounded'>
									<h3>Product Options</h3>
								</a>
							</div>
							<div>
								<a href='category.html' className='box uk-border-rounded'>
									<h3>Video Tutorials</h3>
								</a>
							</div>
							<div>
								<a href='category.html' className='box uk-border-rounded'>
									<h3>Customization</h3>
								</a>
							</div>
							<div>
								<a href='category.html' className='box uk-border-rounded'>
									<h3>Shipping Methods</h3>
								</a>
							</div>
							<div>
								<a href='category.html' className='box uk-border-rounded'>
									<h3>Product Feedback</h3>
								</a>
							</div>
							<div>
								<a href='category.html' className='box uk-border-rounded'>
									<h3>Freaqently Asked Questions</h3>
								</a>
							</div>
							<div>
								<a href='category.html' className='box uk-border-rounded'>
									<h3>Release Notes</h3>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div className='uk-section uk-padding-remove-top uk-padding-remove-bottom'>
					<div className='uk-container'>
						<hr />
					</div>
				</div>
				<div className='uk-section'>
					<div className='uk-container'>
						<div className='uk-child-width-1-2@s text-dark' uk-grid>
							<div>
								<h3>Popular Articles</h3>
								<ul className='uk-list uk-list-large uk-list-divider link-icon-right'>
									<li>
										<a href='article.html'>
											Voluptatem accusantium doloremque laudan rem aperiam
										</a>
									</li>
									<li>
										<a href='article.html'>
											Sorem do eiusmod tempor incididunt dolore magna aliqua
										</a>
									</li>
									<li>
										<a href='article.html'>
											Lorem ipsum dolor sit amet elit sed do eiusmod
										</a>
									</li>
									<li>
										<a href='article.html'>
											Sed ut perspiciatis unde omnis iste natus error sit
											aperiam
										</a>
									</li>
									<li>
										<a href='article.html'>
											Lorem ipsum dolor sit amet, consectetur adipiscing sed
											iusmod
										</a>
									</li>
								</ul>
							</div>
							<div>
								<h3>Recent Articles</h3>
								<ul className='uk-list uk-list-large uk-list-divider link-icon-right'>
									<li>
										<a href='article.html'>
											Lorem ipsum dolor sit ut labore et dolore magna aliqua
										</a>
									</li>
									<li>
										<a href='article.html'>
											Sed ut perspiciatis unde omnis iste natus err aperiam
										</a>
									</li>
									<li>
										<a href='article.html'>
											Consectetur adipiscing elit sed do eiusmod
										</a>
									</li>
									<li>
										<a href='article.html'>
											Donseced do eiusmod tempor incididunt ut labore magna
											aliqua
										</a>
									</li>
									<li>
										<a href='article.html'>
											Sed utem accusantium doloremque laudantium totam
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div
					className='uk-section uk-padding-remove section-cta uk-background-blend-lighten uk-background-center-center uk-background-cover uk-text-center'
					style={{
						backgroundImage:
							'url(https://images.pexels.com/photos/1015568/pexels-photo-1015568.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260)',
					}}
				>
					<div className='uk-background-muted1 uk-border-rounded1 uk-padding-large'>
						<h2>Can't find what you're looking for?</h2>
						<p className='uk-text-lead'>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
							eiusmod tempor.
						</p>
						<p className='uk-margin-medium-top'>
							<a
								href='contact.html'
								className='uk-button uk-button-primary uk-button-large'
							>
								Contact Support
							</a>
						</p>
					</div>
				</div>
				<footer
					id='footer'
					className='uk-section uk-margin-remove uk-section-xsmall uk-text-small uk-text-muted border-top'
				>
					<div className='uk-container'>
						<div className='uk-child-width-1-2@m uk-text-center' uk-grid>
							<div className='uk-text-right@m'>
								<a
									href='#'
									className='uk-icon-link uk-margin-small-right'
									uk-icon='icon: facebook'
								/>
								<a
									href='#'
									className='uk-icon-link uk-margin-small-right'
									uk-icon='icon: google'
								/>
								<a
									href='#'
									className='uk-icon-link uk-margin-small-right'
									uk-icon='icon: vimeo'
								/>
								<a
									href='#'
									className='uk-icon-link uk-margin-small-right'
									uk-icon='icon: instagram'
								/>
								<a
									href='#'
									className='uk-icon-link uk-margin-small-right'
									uk-icon='icon: twitter'
								/>
								<a
									href='#'
									className='uk-icon-link uk-margin-small-right'
									uk-icon='icon: youtube'
								/>
							</div>
							<div className='uk-flex-first@m uk-text-left@m'>
								<p className='uk-text-small'>
									Copyright 2017 Powered by Code Love
								</p>
							</div>
						</div>
					</div>
				</footer>
				<div id='offcanvas' uk-offcanvas='flip: true; overlay: true'>
					<div className='uk-offcanvas-bar'>
						<a
							className='uk-margin-small-bottom uk-logo uk-text-uppercase'
							href='index.html'
						>
							<span
								className='uk-margin-small-right'
								uk-icon='icon: lifesaver'
							/>{' '}
							Knowledge
						</a>
						<ul className='uk-nav uk-nav-default uk-text-uppercase'>
							<li>
								<a href='index.html'>Home</a>
							</li>
							<li className='uk-parent'>
								<a href='article.html'>Article</a>
								<ul className='uk-nav-sub'>
									<li>
										<a href='article.html'>Scrollspy</a>
									</li>
									<li>
										<a href='article-narrow.html'>Narrow</a>
									</li>
								</ul>
							</li>
							<li>
								<a href='faq.html'>Faq</a>
							</li>
							<li>
								<a href='contact.html'>Contact</a>
							</li>
							<li>
								<a href='components.html'>Components</a>
							</li>
						</ul>
						<a
							href='contact.html'
							className='uk-button uk-button-small uk-button-default uk-width-1-1 uk-margin'
						>
							Support
						</a>
						<div className='uk-width-auto uk-text-center'>
							<a
								href='#'
								className='uk-icon-link uk-margin-small-right'
								uk-icon='icon: facebook'
							/>
							<a
								href='#'
								className='uk-icon-link uk-margin-small-right'
								uk-icon='icon: google'
							/>
							<a
								href='#'
								className='uk-icon-link uk-margin-small-right'
								uk-icon='icon: twitter'
							/>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}
