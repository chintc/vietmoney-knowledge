import Link from 'next/link'

export default function Navbar() {
	return (
		<nav className='uk-navbar-container uk-margin uk-navbar-transparent uk-light'>
			<div className='uk-container'>
				<div uk-navbar>
					<div className='uk-navbar-left'>
						<a
							className='uk-navbar-item uk-logo uk-text-uppercase'
							href='index.html'
						>
							<span
								className='uk-margin-small-right'
								uk-icon='icon: lifesaver'
							/>{' '}
							Knowledge
						</a>
					</div>
					<div className='uk-navbar-right'>
						<ul className='uk-navbar-nav uk-text-uppercase uk-visible@m uk-margin-medium-left'>
							<li>
								<a href='index.html'>Home</a>
							</li>
							<li>
								<a href='article.html'>Article</a>
								<div className='uk-navbar-dropdown'>
									<ul className='uk-nav uk-navbar-dropdown-nav'>
										<li>
											<a href='article.html'>Scrollspy</a>
										</li>
										<li>
											<a href='article-narrow.html'>Narrow</a>
										</li>
									</ul>
								</div>
							</li>
							<li>
								<a href='faq.html'>Faq</a>
							</li>
							<li>
								<a href='contact.html'>Contact</a>
							</li>
							<li>
								<a href='components.html'>Components</a>
							</li>
						</ul>
						<a
							className='uk-navbar-toggle uk-hidden@m'
							href='#offcanvas'
							uk-navbar-toggle-icon
							uk-toggle
						/>
					</div>
				</div>
			</div>
		</nav>
	)
}
