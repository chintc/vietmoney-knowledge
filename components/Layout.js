import Footer from './Footer'
import Navbar from './Navbar'

export default function Layout({ children }) {
	return children
	return (
		<div>
			<div
				className='section-hero uk-background-blend-color-burn uk-background-top-center uk-background-cover uk-section-large1 cta'
				style={{
					backgroundImage: 'url("/assets/img/city.jpg")',
				}}
			>
				<Navbar />
				<div className='uk-container hero'>
					<h1 className='uk-heading-primary uk-text-center uk-margin-large-top uk-light'>
						Create a Knowledge Base with Ease
					</h1>
					<p className='uk-text-lead uk-text-center uk-light'>
						Lead volutpat nibh ligula gravida. Magna auctor eget venenatis
						phasellus luctus sodales pulvinar
					</p>
					<div className='uk-flex uk-flex-center uk-inliner'>
						<form className='uk-margin-medium-top uk-margin-xlarge-bottom uk-search uk-search-default'>
							<a href className='uk-search-icon-flip' uk-search-icon />
							<input
								id='autocomplete'
								className='uk-search-input uk-form-large'
								type='search'
								autoComplete='off'
								name='s'
								placeholder='Enter search term here'
							/>
						</form>
					</div>
				</div>
			</div>

			{children}
			<Footer />
		</div>
	)
}
