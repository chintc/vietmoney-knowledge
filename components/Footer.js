export default function Footer() {
	return (
		<>
			<footer
				id='footer'
				className='uk-section uk-margin-remove uk-section-xsmall uk-text-small uk-text-muted border-top'
			>
				<div className='uk-container'>
					<div className='uk-child-width-1-2@m uk-text-center' uk-grid>
						<div className='uk-text-right@m'>
							<a
								href='#'
								className='uk-icon-link uk-margin-small-right'
								uk-icon='icon: facebook'
							/>
							<a
								href='#'
								className='uk-icon-link uk-margin-small-right'
								uk-icon='icon: google'
							/>
							<a
								href='#'
								className='uk-icon-link uk-margin-small-right'
								uk-icon='icon: vimeo'
							/>
							<a
								href='#'
								className='uk-icon-link uk-margin-small-right'
								uk-icon='icon: instagram'
							/>
							<a
								href='#'
								className='uk-icon-link uk-margin-small-right'
								uk-icon='icon: twitter'
							/>
							<a
								href='#'
								className='uk-icon-link uk-margin-small-right'
								uk-icon='icon: youtube'
							/>
						</div>
						<div className='uk-flex-first@m uk-text-left@m'>
							<p className='uk-text-small'>
								Copyright 2017 Powered by Code Love
							</p>
						</div>
					</div>
				</div>
			</footer>

			<div id='offcanvas' uk-offcanvas='flip: true; overlay: true'>
				<div className='uk-offcanvas-bar'>
					<a
						className='uk-margin-small-bottom uk-logo uk-text-uppercase'
						href='index.html'
					>
						{/* <span className='uk-margin-small-right' uk-icon='icon: lifesaver' />{' '} */}
						Knowledge
					</a>
					<ul className='uk-nav uk-nav-default uk-text-uppercase'>
						<li>
							<a href='index.html'>Home</a>
						</li>
						<li className='uk-parent'>
							<a href='article.html'>Article</a>
							<ul className='uk-nav-sub'>
								<li>
									<a href='article.html'>Scrollspy</a>
								</li>
								<li>
									<a href='article-narrow.html'>Narrow</a>
								</li>
							</ul>
						</li>
						<li>
							<a href='faq.html'>Faq</a>
						</li>
						<li>
							<a href='contact.html'>Contact</a>
						</li>
						<li>
							<a href='components.html'>Components</a>
						</li>
					</ul>
					<a
						href='contact.html'
						className='uk-button uk-button-small uk-button-default uk-width-1-1 uk-margin'
					>
						Support
					</a>
					<div className='uk-width-auto uk-text-center'>
						<a
							href='#'
							className='uk-icon-link uk-margin-small-right'
							uk-icon='icon: facebook'
						/>
						<a
							href='#'
							className='uk-icon-link uk-margin-small-right'
							uk-icon='icon: google'
						/>
						<a
							href='#'
							className='uk-icon-link uk-margin-small-right'
							uk-icon='icon: twitter'
						/>
					</div>
				</div>
			</div>
		</>
	)
}
